package org.shawnbutton.romannumerals

class RomanNumeralConverter {

    def convertToRoman(arabicNumber) {
        def symbols = [1000:"M", 900:"CM", 500:"D", 400:"CD", 100:"C", 90:"XC", 50:"L", 40:"XL", 10:"X", 9:"IX", 5:"V", 4:"IV", 1:"I"]

        def result = ""
        for (symbol in symbols) {
            while (arabicNumber >= symbol.key) {
                result = result + symbol.value
                arabicNumber = arabicNumber - symbol.key
            }
        }

        result
    }
}
