package org.shawnbutton.romannumerals

import org.junit.Test
import org.junit.Before

class RomanNumeralConverterTest {

    def converter

    @Before
    public void setUp() {
        converter = new RomanNumeralConverter()
    }

    @Test
    public void test_convert_1_should_become_I() {
        assert converter.convertToRoman(1) == "I"
    }

    @Test
    public void test_convert_2_should_become_II() {
        assert converter.convertToRoman(2) == "II"
    }

    @Test
    public void test_convert_3_should_become_III() {
        assert converter.convertToRoman(3) == "III"
    }

    @Test
    public void test_convert_4_should_become_IV() {
        assert converter.convertToRoman(4) == "IV"
    }

    @Test
    public void test_convert_5_should_become_V() {
        assert converter.convertToRoman(5) == "V"
    }

    @Test
    public void test_convert_6_should_become_VI() {
        assert converter.convertToRoman(6) == "VI"
    }

    @Test
    public void test_convert_7_should_become_VII() {
        assert converter.convertToRoman(7) == "VII"
    }

    @Test
    public void test_convert_9_should_become_IX() {
        assert converter.convertToRoman(9) == "IX"
    }

    @Test
    public void test_convert_10_should_become_X() {
        assert converter.convertToRoman(10) == "X"
    }

    @Test
    public void test_convert_11_should_become_XI() {
        assert converter.convertToRoman(11) == "XI"
    }

    @Test
    public void test_convert_15_should_become_XV() {
        assert converter.convertToRoman(15) == "XV"
    }

    @Test
    public void test_convert_16_should_become_XVI() {
        assert converter.convertToRoman(16) == "XVI"
    }

    @Test
    public void test_convert_19_should_become_XIX() {
        assert converter.convertToRoman(16) == "XVI"
    }

}
