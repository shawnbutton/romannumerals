package org.shawnbutton.romannumerals

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import static org.junit.runners.Parameterized.Parameters

@RunWith(Parameterized)
class RomanNumParameterizedTest {

    def converter
    def input
    def expectedResult

    @Parameters
    static def dataForParameterizedTest() {

        def dataToRunWith = [
                1:"I",
                2:"II",
                3:"III",
                4:"IV",
                5:"V",
                6:"VI",
                7:"VII",
                9:"IX",
                10:"X",
                11:"XI",
                15:"XV",
                16:"XVI",
                19:"XIX",
                20:"XX",
                22:"XXII",
                24:"XXIV",
                28:"XXVIII",
                37:"XXXVII",
                50:"L",
                46:"XLVI",
                74:"LXXIV",
                100:"C",
                90:"XC",
                500:"D",
                400:"CD",
                1000:"M",
                900:"CM",
                1967:"MCMLXVII",
                3214:"MMMCCXIV"
                ]

        return convertMapToCollectionOfArrays(dataToRunWith)
    }

    private static ArrayList convertMapToCollectionOfArrays(dataToRunWith) {
        def data = []
        dataToRunWith.each { testDataElement ->
            def testData = [testDataElement.key, testDataElement.value] as Object[]
            data << testData
        }
        data
    }

    RomanNumParameterizedTest(input, expectedResult) {
        this.input = input
        this.expectedResult = expectedResult
    }

    @Before
    public void setUp() {
        converter = new RomanNumeralConverter()
    }

    @Test
    public void test_convert_1_should_become_I() {
        assert expectedResult == converter.convertToRoman(input)
    }

}
